#!/usr/bin/env python3
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
# Author: Federico Croce, Gustavo H. X. Shiroma, Marco Lavalle
# Copyright 2010-2011, by the California Institute of Technology. ALL RIGHTS
# RESERVED. United States Government Sponsorship acknowledged.
# Any commercial use must be negotiated with the Office of Technology Transfer
# at the California Institute of Technology.
#
# This software may be subject to U.S. export control laws. By accepting this
# software, the user agrees to comply with all applicable U.S.
# export laws and regulations. User has the responsibility to obtain export
# licenses, or other export authority as may be required before exporting such
# information to foreign countries or providing access to foreign persons.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

import plant
import pytest
from config_tests import VRT_IMAGES, \
    JPG_IMAGES, \
    ENVI_IMAGES, \
    S3_IMAGES


def display_image(img):
    plant.display(img, no_show=True)
    for flag_rgb in [True, False]:
        plant.display(img, no_show=True, rgb=flag_rgb)


@pytest.mark.parametrize("img", JPG_IMAGES)
def test_display_jpg(img):
    display_image(img)


@pytest.mark.parametrize("img", ENVI_IMAGES)
def test_display_envi(img):
    display_image(img)


@pytest.mark.parametrize("img", VRT_IMAGES)
def test_display_vrt(img):
    display_image(img)


@pytest.mark.parametrize("img", S3_IMAGES)
def test_display_s3(img):
    display_image(img)
