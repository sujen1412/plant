#!/usr/bin/env python3
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
# Author: Federico Croce, Gustavo H. X. Shiroma, Marco Lavalle
# Copyright 2010-2011, by the California Institute of Technology. ALL RIGHTS
# RESERVED. United States Government Sponsorship acknowledged.
# Any commercial use must be negotiated with the Office of Technology Transfer
# at the California Institute of Technology.
#
# This software may be subject to U.S. export control laws. By accepting this
# software, the user agrees to comply with all applicable U.S.
# export laws and regulations. User has the responsibility to obtain export
# licenses, or other export authority as may be required before exporting such
# information to foreign countries or providing access to foreign persons.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

import plant
from osgeo import gdal
import pytest
from itertools import combinations

from config_tests import VRT_IMAGES


# TODO: Add the nlooks testing
@pytest.mark.parametrize("img", VRT_IMAGES)
@pytest.mark.parametrize("n_bands_to_filter", [1, 2, 3])
# the number of bands to filter
def test_band(img, n_bands_to_filter):
    gdal_img = gdal.Open(img)
    n_bands = gdal_img.RasterCount
    # Syntax: <iterable, n_elements per combination>
    bands_comb = list(combinations(range(n_bands),
                                   n_bands_to_filter))
    for comb in bands_comb:
        band_param = str(comb).replace('(', '').replace(')', '')
        if n_bands_to_filter == 1:
            band_param = band_param.replace(',', '')
        for force_flag in [True, False]:
            for mute_flag in [True, False]:
                for separate_flag in [True, False]:
                    filtered_img = plant.filter(img,
                                                band=band_param,
                                                mute=mute_flag,
                                                force=force_flag,
                                                separate=separate_flag)
                    assert filtered_img.getNBands() == n_bands_to_filter
                    assert gdal_img.RasterXSize == filtered_img.getWidth()
                    assert gdal_img.RasterYSize == filtered_img.getLength()
