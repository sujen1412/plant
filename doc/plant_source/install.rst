Installing PLAnT Python Framework
====================================

Installing Python Framework via pip
-------------------------------------

With python and pip installed in your system. You can install PLAnT with the command:

>>> python -m pip install PLAnT-<version>.tar.gz

To execute the scripts you will need to add the "apps" folder to your path.

Building PLAnT C++ Framework
-------------------------------------

PLAnT uses cmake for building and installing the binaries. To build out-of-source, create a build directory, then run the following command from within the build directory

>>> cmake ../plant

There might be the need to explicitly tell cmake where to find IpOpt by setting the `IPOPT_DIR` variable to the IpOpt root (libipopt is in `$IPOPT_DIR/lib/libipopt.so)`

>>> cmake ../plant -DIPOPT_DIR:PATH=~/anaconda3/

Alternatively, the variable `IPOPT_DIR` can be exported in the user environment before running cmake:

>>> export IPOPT_DIR = ~/anaconda3/
>>> cmake ../plant

The same can be done for GDAL using the variable `GDAL_DIR`.
