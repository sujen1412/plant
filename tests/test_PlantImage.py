import numpy as np
import plant
import pytest
import os

from config_tests import SLC_IMAGES, \
    CONFIG_FILE


def _check_save_and_read(image_obj, var_2d=None,
                         output_format=None):

    print(f'test saving {var_2d} as {output_format}')

    gdal_formats = ['GTIFF', 'ENVI', 'ISCE']
    # other_formats = ['ISCE_NP', 'SRF']
    other_formats = []
    if output_format is None:
        for output_format in gdal_formats+other_formats:
            _check_save_and_read(
                image_obj, var_2d=var_2d,
                output_format=output_format)
        return
        
    # save
    temp_file = plant.get_temporary_file(append=True)
    plant.save_image(image_obj, temp_file,
                     output_format=output_format,
                     force=True)
    assert plant.isfile(temp_file)

    # read as file
    new_image_obj = plant.read_image(temp_file)
    assert new_image_obj is not None
    assert not new_image_obj.image_loaded
    if var_2d is None:
        return new_image_obj
    var_2d_array = np.asarray(var_2d)
    assert np.array_equal(new_image_obj.image, var_2d_array)
    # obj_dtype = plant.get_dtype_name(new_image_obj.image)
    # array_dtype = plant.get_dtype_name(var_2d_array)
    # assert obj_dtype == array_dtype
    assert new_image_obj.image_loaded

    if output_format not in gdal_formats:
        return

    # save (VRT)
    temp_vrt_file = temp_file + '.vrt'
    plant.save_image(temp_file, temp_vrt_file,
                     output_format='VRT',
                     force=True)
    assert plant.isfile(temp_vrt_file)

    # read as file (VRT)
    vrt_image_obj = plant.read_image(temp_vrt_file)
    assert vrt_image_obj is not None
    assert vrt_image_obj.file_format == 'VRT'
    # assert vrt_image_obj.image.dtype == var_2d.dtype
    assert not vrt_image_obj.image_loaded
    assert np.array_equal(vrt_image_obj.image.shape, var_2d_array.shape)
    assert np.array_equal(vrt_image_obj.image, var_2d_array)
    assert vrt_image_obj.image_loaded

    return new_image_obj


def _check_image_obj_bands(image_obj, band=None):
    if band is None:
        band = image_obj.nbands-1
    assert band == image_obj.nbands-1
    for b in range(band):
        image = image_obj.get_image(band=b)
        # print(f'*** {b} {image[0, 0]}')
        assert image[0, 0] == b


def test_bands():

    # create bands
    nbands = 12
    for b in range(nbands):
        if b == 0:
            image_obj = plant.PlantImage(b)
            _check_image_obj_bands(image_obj)
        else:
            image_obj.set_band(b, band=b)
            _check_image_obj_bands(image_obj)

    # copy to a new object
    new_image_obj = plant.PlantImage(image_obj)
    _check_image_obj_bands(image_obj)

    # save and read:
    saved_image_obj = _check_save_and_read(new_image_obj,
                                           output_format='SRF')
    assert isinstance(saved_image_obj, plant.PlantImage)
    assert not saved_image_obj.image_loaded
    # _check_image_obj_bands(saved_image_obj)
    # assert saved_image_obj.image_loaded
    assert saved_image_obj.nbands == nbands
    new_image_2_obj = plant.PlantImage(100)
    bands_to_copy = [3, 5, 10]
    for i, b in enumerate(bands_to_copy):
        band = saved_image_obj.get_band(band=b)
        new_image_2_obj.set_band(band, band=i)
    assert new_image_2_obj.nbands == len(bands_to_copy)

    # for i, b in enumerate(bands_to_copy):
    #    assert new_image_2_obj.get_image(band=i) == b


def test_temporary_files():
    temp_file_1 = plant.get_temporary_file()
    assert temp_file_1
    plant.append_temporary_file(temp_file_1)
    assert temp_file_1 in plant.plant_config.temporary_files
    temp_file_2 = plant.get_temporary_file(append=True)
    assert temp_file_2
    assert temp_file_1 != temp_file_2
    assert temp_file_2 in plant.plant_config.temporary_files


@pytest.mark.parametrize("var", [(3), (3.1), (3 + 1j), ([4, 4])])
def test_read_image_same_output(var):
    # read as number
    var_2d = plant.shape_image(var)
    image_obj = plant.read_image(var)
    assert image_obj is not None
    assert np.array_equal(image_obj.image, var_2d)
    _check_save_and_read(image_obj, var_2d)


@pytest.mark.parametrize("var", [('0:3'), ('0:3:0.5')])
def test_read_image_arange(var):
    args = [float(x) for x in var.split(':')]
    var_2d = plant.shape_image(np.arange(*args))
    assert var_2d is not None
    image_obj = plant.read_image(var)
    assert image_obj is not None
    assert np.array_equal(image_obj.image, var_2d)
    _check_save_and_read(image_obj, var_2d)


@pytest.mark.parametrize("filename", SLC_IMAGES)
def test_create_config_txt(filename):
    config_filename = filename + "_config.txt"
    plant.create_config_txt(config_filename,
                            filename)
    pl_img_shape = plant.read_image(filename).get_shape()
    with open(config_filename) as config_file:
        assert config_file is not None
        checking = pl_img_shape[0]
        n_checked = 0
        for line in config_file:
            assert line is not None
            if n_checked < 2:
                line = line.replace('\n', '').replace('\r', '')
                if line.isdigit():
                    assert checking == int(line)
                    checking = pl_img_shape[1]
                    n_checked += 1
    os.remove(config_filename)


@pytest.mark.parametrize("filepath", CONFIG_FILE)
def test_get_info_from_config(filepath):
    files = [f for f in os.listdir(filepath) if ".txt" not in f]
    for filename in files:
        filename_path = os.path.join(filepath, filename)
        pl_img = plant.get_info_from_config_txt(filename_path)
        with open(os.path.join(filepath, "config.txt"), 'r') as f:
            config_from_file = f.read()
            config_from_file = config_from_file.upper()
            config_from_file = config_from_file.split('\n')
            try:
                ncol_index = config_from_file.index('NCOL')
                width = int(config_from_file[ncol_index+1])
            except:
                width = None
            try:
                nrow_index = config_from_file.index('NROW')
                length = int(config_from_file[nrow_index+1])
            except:
                length = None
        assert pl_img is not None
        # TODO: Due to a bug with the PlantImage constructor, the following assertions fail, they should pass
        # assert pl_img.getWidth() == width
        # assert pl_img.getLength() == length
