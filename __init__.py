# import plant.modules.plant_config
import sys


class Singleton(type):
    _instances = {}

    def __call__(cls, *args, **kwargs):
        if cls not in cls._instances:
            cls._instances[cls] = super(Singleton, cls).__call__(*args,
                                                                 **kwargs)
        return cls._instances[cls]


alias_dict = {'d': 'display',
              'u': 'plant_util.py',
              'i': 'plant_info.py',
              'f': 'plant_filter.py',
              'l': 'plant_ls.py',
              'm': 'plant_mosaic.py'}


class NameWrapper(object):
    def __init__(self, wrapped):
        self.wrapped = wrapped

    def __getattr__(self, name):
        try:
            return getattr(self.wrapped, name)
        except AttributeError:
            pass
        if name == '__version__':
            return plant.VERSION
        if name in alias_dict.keys():
            name = alias_dict[name]
        return plant.ModuleWrapper(name)

    def __dir__(self):
        return plant.__dir__()


from .modules import *
# from .app import *

'''
if plant.MATPLOTLIB_BACKEND is not None or sys.platform == 'darwin':
    import matplotlib as mpl
    if plant.MATPLOTLIB_BACKEND is not None:
        mpl.use(plant.MATPLOTLIB_BACKEND)
    elif int(mpl.__version__.split('.')[0]) >= 3:
        mpl.use("TkAgg")
        flag_error = False
        try:
            import matplotlib.pyplot as plt
        except:
            flag_error = True
        if flag_error and sys.platform == 'darwin':
            mpl.use("macosx")
        elif flag_error:
            mpl.use("GTK3Agg")
'''

sys.modules[__name__] = NameWrapper(sys.modules[__name__])

plant_config.reset()

if plant_config.logger_obj is None:
    plant_config.logger_obj = PlantLogger()

# locals().update(plant_config.__dict__)
