#!/usr/bin/env python3
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
# Author: Federico Croce, Gustavo H. X. Shiroma, Marco Lavalle
# Copyright 2010-2011, by the California Institute of Technology. ALL RIGHTS
# RESERVED. United States Government Sponsorship acknowledged.
# Any commercial use must be negotiated with the Office of Technology Transfer
# at the California Institute of Technology.
#
# This software may be subject to U.S. export control laws. By accepting this
# software, the user agrees to comply with all applicable U.S.
# export laws and regulations. User has the responsibility to obtain export
# licenses, or other export authority as may be required before exporting such
# information to foreign countries or providing access to foreign persons.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


import numpy as np
import plant
import datetime
import pytest

FILTER_MIN_ACCURACY = 1e-8


def test_filter():
    filter_size = 3
    image = np.zeros((2*filter_size+1, 2*filter_size+1))
    p1 = int(filter_size/2)
    p2 = int(filter_size+1)
    image[p1, p1] = filter_size**2
    image[p2, p2] = filter_size**2-1
    image[p2+1, p2+1] = np.nan
    out_image = plant.filter_data(image, boxcar=filter_size)
    assert out_image is not None
    assert abs(out_image[p1, p1] - 1.0) < FILTER_MIN_ACCURACY
    assert abs(out_image[p1+1, p1+1] - 1.0) < FILTER_MIN_ACCURACY
    assert abs(out_image[2*filter_size, 0]) < FILTER_MIN_ACCURACY
    assert abs(out_image[p2, p2] - 1.0) < FILTER_MIN_ACCURACY
    assert abs(out_image[p2+1, p2+1] - 1.0) < FILTER_MIN_ACCURACY


@pytest.mark.parametrize("data", [[2000, 1, 1], [0, 1, 1], [2000, -2, 2]])
def test_datetime_to_array(data):
    # TODO: add matplotlib testing, parameter of the plant.datetime_to_array
    flag_fail = False
    for item in data:
        if item <= 0:
            flag_fail = True
    if flag_fail:
        with pytest.raises(ValueError) as excinfo:
            datetime.datetime(data[0], data[1], data[2])
            assert excinfo.value is not None
    else:
        inp = datetime.datetime(data[0], data[1], data[2])
        np_arr = plant.datetime_to_array(inp)

        assert np_arr is not None
        assert np_arr[0] == pytest.approx(9.467136e+08, 0.1)
        assert np_arr.shape == (1,)

