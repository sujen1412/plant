#!/usr/bin/env python3
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
# Author: Federico Croce, Gustavo H. X. Shiroma, Marco Lavalle
# Copyright 2010-2011, by the California Institute of Technology. ALL RIGHTS
# RESERVED. United States Government Sponsorship acknowledged.
# Any commercial use must be negotiated with the Office of Technology Transfer
# at the California Institute of Technology.
#
# This software may be subject to U.S. export control laws. By accepting this
# software, the user agrees to comply with all applicable U.S.
# export laws and regulations. User has the responsibility to obtain export
# licenses, or other export authority as may be required before exporting such
# information to foreign countries or providing access to foreign persons.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


import os

DATA_DIR = "data"

JPG_FOLDER = "jpg"
VRT_FOLDER = "vrt"
ENVI_FOLDER = "envi"
S3_FOLDER = "s3"
SLC_FOLDER = "slc"
CSV_FOLDER = "csv"
CONFIG_TXT_FOLDER = "config_file"
SHAPEFILES_FOLDER = "shapefiles"

SUPPORTED_EXTENSIONS = ["jpg", "envi", "slc", "bin"]

CONFIG_FILE = [os.path.join(DATA_DIR, CONFIG_TXT_FOLDER)]

JPG_IMAGES = [os.path.join(DATA_DIR, JPG_FOLDER, filename)
              for filename in os.listdir(os.path.join(DATA_DIR, JPG_FOLDER))]
VRT_IMAGES = [os.path.join(DATA_DIR, VRT_FOLDER, filename)
              for filename in os.listdir(os.path.join(DATA_DIR, VRT_FOLDER))
              if filename.endswith(".vrt")]
ENVI_IMAGES = [os.path.join(DATA_DIR, ENVI_FOLDER, filename)
               for filename in os.listdir(os.path.join(DATA_DIR, ENVI_FOLDER))
               if filename.endswith(".envi")]
S3_IMAGES = [os.path.join(DATA_DIR, S3_FOLDER, filename)
             for filename in os.listdir(os.path.join(DATA_DIR, S3_FOLDER))]
SLC_IMAGES = [os.path.join(DATA_DIR, SLC_FOLDER, filename)
              for filename in os.listdir(os.path.join(DATA_DIR, SLC_FOLDER))
              if filename.endswith(".slc")]
CSV_FILES = [os.path.join(DATA_DIR, CSV_FOLDER, filename)
             for filename in os.listdir(os.path.join(DATA_DIR, CSV_FOLDER))]
SHAPEFILES_IMAGES = [os.path.join(DATA_DIR, SHAPEFILES_FOLDER, filename)
                     for filename in
                     os.listdir(os.path.join(DATA_DIR, SHAPEFILES_FOLDER))
                     if filename.endswith(".shp")]
