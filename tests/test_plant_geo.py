import plant

lat_end = 34.380885
lon_beg = -118.847848
lat_beg = 33.491165
lon_end = -117.506953
lat_arr = [lat_beg, lat_end]
lon_arr = [lon_beg, lon_end]
step_lat = 0.0001
step_lon = 0.0002


def test_get_coordinates():
    ret_dict = plant.get_coordinates(lat_arr=lat_arr,
                                     lon_arr=lon_arr,
                                     step_lat=step_lat,
                                     step_lon=step_lon)
    out_lat_arr = ret_dict['lat_arr']
    out_lon_arr = ret_dict['lon_arr']
    out_step_lat = ret_dict['step_lat']
    out_step_lon = ret_dict['step_lon']
    assert lat_arr == out_lat_arr
    assert lon_arr == out_lon_arr
    assert step_lat == out_step_lat
    assert step_lon == out_step_lon


def test_get_geotransform():
    geotransform = plant.get_geotransform(lat_arr=lat_arr,
                                          lon_arr=lon_arr,
                                          step_lat=step_lat,
                                          step_lon=step_lon)
    ret_dict = plant.get_coordinates(geotransform=geotransform)
    out_lat_arr = ret_dict['lat_arr']
    out_lon_arr = ret_dict['lon_arr']
    out_step_lat = ret_dict['step_lat']
    out_step_lon = ret_dict['step_lon']
    assert lat_arr == out_lat_arr
    assert lon_arr == out_lon_arr
    assert step_lat == out_step_lat
    assert step_lon == out_step_lon
