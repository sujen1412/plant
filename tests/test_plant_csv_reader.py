#!/usr/bin/env python3
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
# Author: Federico Croce, Gustavo H. X. Shiroma, Marco Lavalle
# Copyright 2010-2011, by the California Institute of Technology. ALL RIGHTS
# RESERVED. United States Government Sponsorship acknowledged.
# Any commercial use must be negotiated with the Office of Technology Transfer
# at the California Institute of Technology.
#
# This software may be subject to U.S. export control laws. By accepting this
# software, the user agrees to comply with all applicable U.S.
# export laws and regulations. User has the responsibility to obtain export
# licenses, or other export authority as may be required before exporting such
# information to foreign countries or providing access to foreign persons.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

import plant
import time
import gdal
import pytest
import os
from config_tests import CSV_FILES
from config_tests import SUPPORTED_EXTENSIONS
from config_tests import DATA_DIR


@pytest.mark.parametrize("file", CSV_FILES)
def test_lvis(file):
    for ext in SUPPORTED_EXTENSIONS:
        output_filename = os.path.join(DATA_DIR, str(time.time()) + "." + ext)
        plant.csv_reader(file,
                         output_file=output_filename,
                         lon_label='GLON', lat_label='GLAT',
                         data_label='ZG',
                         step_m=20,
                         lvis=True)
        pl_img = plant.read_image(output_filename)
        gdal_img = gdal.Open(output_filename)
        assert gdal_img.RasterCount == pl_img.getNBands()
        assert gdal_img.RasterXSize == pl_img.getWidth()
        assert gdal_img.RasterYSize == pl_img.getLength()
        os.remove(output_filename)
