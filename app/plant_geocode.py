#!/usr/bin/env python3
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
# Author: Gustavo H. X. Shiroma, Marco Lavalle
# Copyright 2010-2011, by the California Institute of Technology. ALL RIGHTS
# RESERVED. United States Government Sponsorship acknowledged.
# Any commercial use must be negotiated with the Office of Technology Transfer
# at the California Institute of Technology.
#
# This software may be subject to U.S. export control laws. By accepting this
# software, the user agrees to comply with all applicable U.S.
# export laws and regulations. User has the responsibility to obtain export
# licenses, or other export authority as may be required before exporting such
# information to foreign countries or providing access to foreign persons.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# import time
from os import path
from plant.app.plant_mosaic import PlantMosaic
import copy
import plant


def get_parser():
    '''
    Command line parser.
    '''
    descr = 'Geocode images using geolocation arrays.'

    epilog = ''
    parser = plant.argparse(epilog=epilog,
                            description=descr,
                            geo=1,
                            topo_dir=1,
                            backward_geocoding=1,
                            input_images=2,
                            default_geo_input_options=1,
                            default_geo_output_options=1,
                            output_file=1,
                            output_dir=1,
                            separate=1,
                            default_options=1)
    # geo_multilook=1)

    parser.add_argument('--factor', '--factors', '--multiplier',
                        '--multipliers',
                        dest='factors',
                        type=float,
                        nargs='+',
                        help='Factors to be applied '
                        'on inputs')

    # parameters
    parser.add_argument('--transition',
                        dest='transition',
                        type=float,
                        help='Defines transition width in overlapping'
                        'areas (default: %(default)s).',
                        default=0)
    parser.add_argument('--interp', '--interp-method',
                        dest='interp_method',
                        type=str,
                        help='Interpolation method '
                        'for geocoding (Options: near, bilinear, cubic, '
                        'cubicspline, lanczos, '
                        'average, mode, max, min, med, q1, q3)')

    parser.add_argument('--gdal-warp-list', '--gdalwarp-list',
                        dest='gdalwarp_list',
                        action='store_true',
                        help='Use gdalwarp list (faster)')

    parser.add_argument('--any-null', '--any-nan',
                        dest='any_null',
                        action='store_true',
                        help='Accumulate NaNs to'
                        'the output')
    parser.add_argument('--no-average',
                        dest='no_average',
                        action='store_true',
                        help='Do not average in overlapping '
                        'area (choose the first data).')
    parser.add_argument('--src-no-geotransform',
                        dest='no_geotransform_src',
                        action='store_true',
                        help='Set GDAL keyword '
                        '-to SRC_METHOD=NO_GEOTRANSFORM.')
    parser.add_argument('--dst-no-geotransform',
                        dest='no_geotransform_dst',
                        action='store_true',
                        help='Set GDAL keyword '
                        '-to DST_METHOD=NO_GEOTRANSFORM.')

    parser.add_argument('--srtm',
                        dest='srtm',
                        action='store_true',
                        help='Use SRTM conventions (NULL etc.)')

    return parser


class PlantGeocoding(PlantMosaic):

    def run(self):
        '''
        Run geocoding
        '''
        self_mosaic_method = self.mosaic_geo_gdalwarp
        self.print('transition width: '+str(self.transition))

        if self.separate:
            self_dict = {}
            prevent_copy_list = ['parser']
            for key, value in self.__dict__.items():
                if key in prevent_copy_list:
                    continue
                self_dict[key] = copy.deepcopy(value)
            input_images = self.input_images
            # topo_dir_list = self.topo_dir_list
            # backward_geocoding_x_list = self.backward_geocoding_x_list
            # backward_geocoding_y_list = self.backward_geocoding_y_list
            image_obj = None
            current_band = 0
            for i, current_file in enumerate(input_images):
                if i != 0:
                    plant.plant_config.logger_obj.flush_temporary_files()
                    self.__dict__.update(self_dict)
                self.input_images = [current_file]
                # self.topo_dir_list = [topo_dir_list[i]]
                # self.backward_geocoding_x_list =
                # [backward_geocoding_x_list[i]]
                # self.backward_geocoding_y_list =
                # [backward_geocoding_y_list[i]]
                if self.output_dir_orig is None:
                    temp_file = plant.get_temporary_file(
                        current_file + '.geo_sep_' + str(i),
                        dirname=plant.dirname(self.output_file),
                        append=True)
                    self.output_file = temp_file
                else:
                    self.output_file = self.output_files[i]
                if (self.output_skip_if_existent and
                        plant.isfile(self.output_file)):
                    print(f'INFO output file {self.output_file}'
                          ' already exist, skipping execution..')
                    continue
                if image_obj is None:
                    image_obj = self_mosaic_method()
                    current_band = image_obj.nbands
                else:
                    current_image_obj = self_mosaic_method()
                    for band in range(current_image_obj.nbands):
                        image = current_image_obj.get_image(band=band)
                        image_obj.set_image(image, band=current_band)
                        current_band += 1
            plant.plant_config.logger_obj.flush_temporary_files()
            self.__dict__.update(self_dict)
            if self.output_dir_orig is None:
                self.save_image(image_obj, self.output_file)
            return image_obj
        if not self.output_file:
            self.output_file = path.join(self.output_dir, 'mosaic.bin')
        return self_mosaic_method()


def main(argv=None):
    with plant.PlantLogger():
        parser = get_parser()
        self_obj = PlantGeocoding(parser, argv)
        ret = self_obj.run()
        return ret


if __name__ == '__main__':
    main()
