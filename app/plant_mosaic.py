#!/usr/bin/env python3
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
# Author: Gustavo H. X. Shiroma, Marco Lavalle
# Copyright 2010-2011, by the California Institute of Technology. ALL RIGHTS
# RESERVED. United States Government Sponsorship acknowledged.
# Any commercial use must be negotiated with the Office of Technology Transfer
# at the California Institute of Technology.
#
# This software may be subject to U.S. export control laws. By accepting this
# software, the user agrees to comply with all applicable U.S.
# export laws and regulations. User has the responsibility to obtain export
# licenses, or other export authority as may be required before exporting such
# information to foreign countries or providing access to foreign persons.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

import time
import sys
import numpy as np
from os import path, makedirs

import gdal
# from gdalconst import GA_ReadOnly
import copy
import plant

FLAG_REPROJECT_WITH_GDALWARP = True
# DEFAULT_GDAL_FORMAT = 'ENVI'
# DEFAULT_GDAL_EXTENSION = '.bin'


def get_parser():
    '''
    Command line parser.
    '''
    descr = ('Mosaic or geographically relocate input data.'
             ' If a single input is used, the input can be'
             ' relocated using one of the bounding box/step'
             ' options. If multiple inputs are used'
             ' the files are mosaicked to a single-output.')

    epilog = ''
    parser = plant.argparse(epilog=epilog,
                            description=descr,
                            geo=1,
                            # topo_dir=1,
                            # flag_debug=1,
                            input_images=2,
                            default_geo_input_options=1,
                            default_geo_output_options=1,
                            output_file=1,
                            output_dir=1,
                            separate=1,
                            default_options=1)
    # geo_multilook=1)

    parser.add_argument('--factor', '--factors', '--multiplier',
                        '--multipliers',
                        dest='factors',
                        type=float,
                        nargs='+',
                        help='Factors to be applied '
                        'on inputs')

    # parameters
    parser.add_argument('--transition',
                        dest='transition',
                        type=float,
                        help='Defines transition width in overlapping'
                        'areas (default: %(default)s).',
                        default=0)
    parser.add_argument('--interp', '--interp-method',
                        dest='interp_method',
                        type=str,
                        help='Interpolation method '
                        'for geocoding (Options: near, bilinear, cubic, '
                        'cubicspline, lanczos, '
                        'average, mode, max, min, med, q1, q3)')

    parser.add_argument('--srtm',
                        dest='srtm',
                        action='store_true',
                        help='Use SRTM conventions (NULL etc.)')

    parser.add_argument('--gdal-warp-list', '--gdalwarp-list',
                        dest='gdalwarp_list',
                        action='store_true',
                        help='Use gdalwarp list (faster)')

    parser.add_argument('--any-null', '--any-nan',
                        dest='any_null',
                        action='store_true',
                        help='Accumulate NaNs to'
                        'the output')
    parser.add_argument('--no-average',
                        dest='no_average',
                        action='store_true',
                        help='Do not average in overlapping '
                        'area (choose the first data).')
    parser.add_argument('--src-no-geotransform',
                        dest='no_geotransform_src',
                        action='store_true',
                        help='Set GDAL keyword '
                        '-to SRC_METHOD=NO_GEOTRANSFORM.')
    parser.add_argument('--dst-no-geotransform',
                        dest='no_geotransform_dst',
                        action='store_true',
                        help='Set GDAL keyword '
                        '-to DST_METHOD=NO_GEOTRANSFORM.')
    # parser.add_argument('--average', dest='no_average', action='store_false',
    #                    required=False, help='Do not average in overlapping '
    #                    'area (choose the first data).')
    return parser


class PlantMosaic(plant.PlantScript):

    def __init__(self, parser, argv=None):
        '''
        class initialization
        '''
        self.projection = None
        super().__init__(parser, argv)
        self.output_width = None
        self.math_mode = False
        self.populate_parameters()

    def populate_parameters(self):
        """
        populate class's attributes
        """
        if self.srtm and self.in_null is None:
            self.in_null = -32768

        if 'topo_dir' not in self.__dict__.keys():
            self.topo_dir = None
        if 'backward_geocoding_x' not in self.__dict__.keys():
            self.backward_geocoding_x = None
        if 'backward_geocoding_y' not in self.__dict__.keys():
            self.backward_geocoding_y = None
        lat_size = None
        lon_size = None
        if not self.output_file and not self.output_dir:
            self.parser.print_usage()
            self.print('ERROR please select an output file or directory')
            sys.exit(1)

        if path.isdir(self.output_file) or self.output_file.endswith('/'):
            self.output_file = ''

        output_projection_ref = None
        if (self.bbox is None and self.bbox_file):
            geo_file = self.bbox_file
            output_projection_ref = geo_file
        elif self.backward_geocoding_x:
            geo_file = [self.backward_geocoding_x]
            output_projection_ref = geo_file[0]
        elif self.bbox is None:
            geo_file = self.input_images
            output_projection_ref = geo_file[0]
        elif self.bbox is not None:
            geo_file = None
            output_projection_ref = self.bbox_file

        if (self.output_projection is None and
                self.projection is not None):
            self.output_projection
        elif (self.output_projection is None and
              output_projection_ref):
            projection = plant.read_image(output_projection_ref,
                                          only_header=True).projection
            self.output_projection = projection
        
        if not self.output_projection:
            # self.output_projection = ('+proj=longlat +ellps=WGS84 '
            #                           '+datum=WGS84 +no_defs')  # t_srs
            self.output_projection = plant.PROJECTION_REF

        self.use_gdal_bbox = (self.bbox is None and
                              self.bbox_file is None and
                              len(self.input_images) <= 1 and
                              not self.backward_geocoding_x)
        self.use_gdal_step = (self.step_lon is None and
                              self.step_lat is None and
                              self.step is None and
                              len(self.input_images) <= 1 and
                              not self.backward_geocoding_x)

        if not self.gdalwarp_list:
            flag_use_gdalwarp_list = True
            for i, current_file_1 in enumerate(self.input_images):
                if not flag_use_gdalwarp_list:
                    break
                for j, current_file_2 in enumerate(self.input_images):
                    if i <= j:
                        continue
                    image_1_obj = self.read_image(current_file_1,
                                                  only_header=True,
                                                  # topo_dir=topo_dir_list[i],
                                                  verbose=False)
                    image_2_obj = self.read_image(current_file_2,
                                                  only_header=True,
                                                  # topo_dir=topo_dir_list[j],
                                                  verbose=False)
                    if self.test_overlap(image_obj=image_1_obj,
                                         image_2_obj=image_2_obj):
                        flag_use_gdalwarp_list = False
                        break
            if flag_use_gdalwarp_list:
                self.gdalwarp_list = True
                if len(self.input_images) > 1:
                    self.print('INFO inputs do not overlap, '
                               'using --gdalwarp_list mode..')

        plant_grid = plant.PlantGrid(
            bbox=self.bbox,
            step=self.step,
            bbox_file=geo_file,
            # verbose=True,
            verbose=False,
            default_step=None,
            step_lon=self.step_lon,
            step_lat=self.step_lat,
            projection=self.projection,
            # bbox_topo=self.topo_dir_list,
            plant_transform_obj=self.plant_transform_obj)

        plant_grid.projection = self.output_projection
        plant_grid.print_geoinformation()

        if plant_grid.projection is not None:
            self.projection = plant_grid.projection
            # projection = ret_dict['projection']
        if (plant_grid.lat_arr is not None and
                plant_grid.lon_arr is not None):
            self.bbox = plant.get_bbox(plant_grid.lat_arr,
                                       plant_grid.lon_arr)
        if plant_grid.step_lat is not None:
            self.step_lat = plant_grid.step_lat
        if plant_grid.step_lon is not None:
            self.step_lon = plant_grid.step_lon
        if plant_grid.geotransform is not None:
            self.geotransform = plant_grid.geotransform
        lon_size = plant_grid.lon_size
        lat_size = plant_grid.lat_size
        if plant_grid.lat_arr is not None:
            self.lat_arr = plant_grid.lat_arr
        if plant_grid.lon_arr is not None:
            self.lon_arr = plant_grid.lon_arr
        self.math_mode = self.factors is not None
        if self.factors is None:
            self.factors = 1
        if self.math_mode:
            self.print('math mode: on')
            self.print('factors: %s' % str(self.factors))
            self.no_average = False
            self.transition = 0

        if self.use_gdal_bbox or self.use_gdal_step:
            # file size will be defined by GDAL
            return
        if self.output_dtype:
            max_datatype_size = plant.get_dtype_size(self.output_dtype)
        else:
            max_datatype_size = 0
            for current_file in self.input_images:
                image_obj = self.read_image(current_file,
                                            verbose=False,
                                            only_header=True)
                if image_obj is not None:
                    current_dtype_size = plant.get_dtype_size(image_obj.dtype)
                    if current_dtype_size > max_datatype_size:
                        max_datatype_size = current_dtype_size
            if max_datatype_size == 0:
                max_datatype_size = 8
        if (lat_size is None or not np.isfinite(lat_size) or
                lon_size is None or not np.isfinite(lon_size)):
            return

        data_size = lon_size*lat_size*max_datatype_size
        if data_size > 5e8:
            while not self.force:
                self.print('WARNING this file seems to be too large (%s) '
                           'for step=(%f,%f) and given coordinates. '
                           % (plant.get_file_size_string(data_size),
                              self.step_lat, self.step_lon))
                res = plant.get_keys('Continue anyway? ([y]es/[n]o) ')
                if res.startswith('y'):
                    break
                elif res.startswith('n'):
                    self.print('operation cancelled by the user.', 1)
                    sys.exit(0)
        else:
            self.print('expected output size: %s' %
                       plant.get_file_size_string(data_size))

    def run(self):
        '''
        Run mosaicking
        '''
        self_mosaic_method = self.mosaic_geo_gdalwarp
        self.print('transition width: '+str(self.transition))
        if self.separate:
            self_dict = {}
            prevent_copy_list = ['parser']
            for key, value in self.__dict__.items():
                if key in prevent_copy_list:
                    continue
                self_dict[key] = copy.deepcopy(value)
            input_images = self.input_images
            image_obj = None
            current_band = 0
            for i, current_file in enumerate(input_images):
                if i != 0:
                    plant.plant_config.logger_obj.flush_temporary_files()
                    self.__dict__.update(self_dict)
                self.input_images = [current_file]
                if self.output_dir_orig is None:
                    temp_file = plant.get_temporary_file(
                        current_file + '.geo_sep_' + str(i),
                        dirname=plant.dirname(self.output_file),
                        append=True)
                    self.output_file = temp_file
                else:
                    self.output_file = self.output_files[i]
                if (self.output_skip_if_existent and
                        plant.isfile(self.output_file)):
                    print(f'INFO output file {self.output_file}'
                          ' already exist, skipping execution..')
                    continue
                if image_obj is None:
                    image_obj = self_mosaic_method()
                    current_band = image_obj.nbands
                else:
                    current_image_obj = self_mosaic_method()
                    for band in range(current_image_obj.nbands):
                        image = current_image_obj.get_image(band=band)
                        image_obj.set_image(image, band=current_band)
                        current_band += 1
            plant.plant_config.logger_obj.flush_temporary_files()
            self.__dict__.update(self_dict)
            if self.output_dir_orig is None:
                self.save_image(image_obj, self.output_file)
            return image_obj
        if not self.output_file:
            self.output_file = path.join(self.output_dir, 'mosaic.bin')
        return self_mosaic_method()

    def mosaic_geo_gdalwarp(self):
        if not path.isdir(plant.dirname(self.output_file)):
            makedirs(plant.dirname(self.output_file))
        gdal_filelist = []
        bands_list = []
        self.input_format_orig = self.input_format
        for i, current_file in enumerate(self.input_images):
            self.print(f'## file {i+1}: {current_file}')
            with plant.PlantIndent():
                self._prepare_file(i, current_file, gdal_filelist,
                                   bands_list)
        self.edges_center = None
        self.edges_outer = None
        self.plant_transform_obj = None

        if len(gdal_filelist) == 0:
            if self.input_images is None:
                self.input_images = 'None'
            if len(self.input_images) >= 1:
                self.print('ERROR invalid input(s): ' +
                           ', '.join(self.input_images))
            else:
                self.print('ERROR input file(s) not found')
            return
        if self.gdalwarp_list:
            geo_file = plant.get_temporary_file(
                path.basename(self.output_file + '.geo_temp_3'),
                dirname=plant.dirname(self.output_file),
                append=True)
            geo_file = self.gdalwarp_binding(gdal_filelist,
                                             geo_file,
                                             include_mask_band=False)
            gdal_filelist = [geo_file]

        if self.use_gdal_bbox or self.use_gdal_step:
            # self.print("no geometry set. Using GDAL's default")
            data_obj = self.read_image(gdal_filelist[0])
            if self.gdalwarp_list:
                bands = data_obj.nbands
            else:
                bands = data_obj.nbands-1
            self.geotransform = data_obj.geotransform
            self.projection = data_obj.projection
            if self.geotransform is not None:
                ret_dict = plant.get_coordinates(
                    geotransform=self.geotransform,
                    verbose=True)
                lat_arr = ret_dict['lat_arr']
                lon_arr = ret_dict['lon_arr']
                step_lat = ret_dict['step_lat']
                step_lon = ret_dict['step_lon']
                # lat_size = ret_dict['lat_size']
                # lon_size = ret_dict['lon_size']
            if bands == 1:  # and
                # self.nlooks_lon <= 1 and
                # self.nlooks_lat <= 1):  # ) or self.geotransform is None)
                data_obj.nbands = 1  # removes mask band
                self.save_image(data_obj, self.output_file)
                return data_obj
            if self.use_gdal_bbox and self.geotransform is not None:
                self.bbox = plant.get_bbox(lat_arr, lon_arr)
            if self.use_gdal_step and self.geotransform is not None:
                self.step_lat = step_lat
                self.step_lon = step_lon

        if (self.gdalwarp_list or len(gdal_filelist) == 1):
            data_obj = self.read_image(gdal_filelist[0],
                                       input_format=plant.DEFAULT_GDAL_FORMAT)
            if not self.gdalwarp_list:
                data_obj.nbands = data_obj.nbands - 1  # removes mask band
            self.save_image(data_obj, self.output_file)
            return data_obj
            # data = data_obj.image
            # indexes = []
        # mosaic selected bands

        data_obj = self.read_image(gdal_filelist[0],
                                   input_format=plant.DEFAULT_GDAL_FORMAT,
                                   only_header=True)

        if self.no_average and not self.math_mode:
            data_obj = self.read_image(gdal_filelist[0],
                                       input_format=plant.DEFAULT_GDAL_FORMAT,
                                       only_header=True)
            data = self.mosaic_no_average(gdal_filelist,
                                          bands_list)
            # data_obj.set_image(data, band=0)
            # data_obj.nbands = 1
            # self.save_image(data_obj, self.output_file)
            # return data_obj
        else:
            data = self.mosaic_average(gdal_filelist,
                                       bands_list)
        data_obj.set_image(data, band=0)
        data_obj.nbands = 1
        self.save_image(data_obj, self.output_file)
        data_obj.realize_changes()
        return data_obj

    def _prepare_file(self, i, current_file, gdal_filelist, bands_list):
        self.input_format = self.input_format_orig
        band = None
        flag_test_gdal_open = plant.test_gdal_open(current_file,
                                                   geo=True)
        if ((not flag_test_gdal_open) and
                (not plant.test_other_drivers(current_file)) and
                plant.IMAGE_NAME_SEPARATOR in current_file):
            current_file_splitted = \
                current_file.split(plant.IMAGE_NAME_SEPARATOR)
            current_file = current_file_splitted[0]
            band = int(current_file_splitted[1])
        suffix = str(time.time())+'_'+str(i)
        if current_file.endswith('.xml'):
            return
        if len(self.input_images) > 1:
            self.print('current file: '+path.basename(current_file))

        temp_file = plant.get_temporary_file(
            current_file + '.geo_temp_' + suffix,
            dirname=plant.dirname(self.output_file),
            append=True)

        if self.backward_geocoding_x or self.backward_geocoding_y:
            with plant.PlantIndent():
                if not self.backward_geocoding_x:
                    self.print('ERROR backward geocoding image X not provided'
                               f' for {current_file}')
                if not self.backward_geocoding_y:
                    self.print('ERROR backward geocoding image Y not provided'
                               f' for {current_file}')
                self.print('backward geocoding image X:'
                           f' {self.backward_geocoding_x}')
                image_x_obj = plant.read_image(self.backward_geocoding_x)
                plant.apply_crop(image_x_obj,
                                 plant_transform_obj=self.plant_transform_obj,
                                 verbose=False)
                self.print('backward geocoding image y:'
                           f' {self.backward_geocoding_y}')
                image_y_obj = plant.read_image(self.backward_geocoding_y)
                plant.apply_crop(image_y_obj,
                                 plant_transform_obj=self.plant_transform_obj,
                                 verbose=False)
                image_obj = image_x_obj.copy()
                image_sr_obj = plant.read_image(current_file)
                valid_ind = np.where(
                    np.logical_and(image_y_obj.image != 55537,
                                   image_x_obj.image != 55537))
                x_ind = image_x_obj.image[valid_ind]
                y_ind = image_y_obj.image[valid_ind]
                for b in range(image_sr_obj.nbands):
                    image = image_sr_obj.get_image(band=b)
                    new_image = np.full((image_x_obj.shape), np.nan,
                                        dtype=image.dtype)
                    new_image[valid_ind] = image[y_ind, x_ind]
                    image_obj.set_image(new_image, band=b)
                # transformation
                plant.apply_null(image_obj,
                                 in_null=self.in_null,
                                 out_null=self.out_null)
                plant.apply_mask(image_obj,
                                 plant_transform_obj=self.plant_transform_obj,
                                 verbose=self.verbose,
                                 force=self.force)
        else:
            image_obj = self.read_image(current_file,
                                        verbose=False,
                                        only_header=True)
            if (not(image_obj.geotransform is not None or
                    image_obj.gcp_projection is not None) and
                    not self.topo_dir):
                self.print('ERROR georeference of %s not found'
                           % current_file)
                return
            flag_apply_transformation = \
                image_obj.plant_transform_obj.flag_apply_transformation()

        if not self.projection and image_obj.projection:
            self.projection = image_obj.projection
        # if (plant.is_isce_image(current_file) and
        #        not(plant.isfile(current_file+'.vrt')) and
        #        plant.isfile(current_file+'.xml')):
        #    self.print('WARNING .vrt header header not found '
        #               'for %s' %(current_file))
        #    fix_vrt(current_file,
        #            force=self.force,

        # self.print(f'*** file format: {image_obj.file_format}')

        # image needs to be backward-geocoded:
        if self.backward_geocoding_x and self.backward_geocoding_y:
            self.save_image(image_obj,
                            temp_file,
                            output_format=plant.DEFAULT_GDAL_FORMAT,
                            flag_temporary=True,
                            force=True)
            # plant.plant_config.temporary_files.append(temp_file)
            current_file = temp_file
        # image needs to be forward-geocoded:
        elif self.topo_dir:
            current_file = self._make_gdal_readable(
                current_file,
                flag_apply_transformation,
                flag_test_gdal_open,
                temp_file,
                band)
            self.print('assigning geolocation to image...')


            plant_transform_topo_obj = plant.PlantTransform(
                select_row=self.plant_transform_obj.select_row,
                select_col=self.plant_transform_obj.select_col,
                srcwin=self.plant_transform_obj.srcwin,
                polygon=self.plant_transform_obj.polygon,
                length_orig=self.plant_transform_obj._length_orig,
                width_orig=self.plant_transform_obj._width_orig)



            with plant.PlantIndent():
                current_file = plant.assign_georeferences(
                    image_obj,
                    topo_dir=self.topo_dir,
                    lat_file=self.lat_file,
                    lon_file=self.lon_file,
                    output_file=current_file,
                    plant_transform_obj=plant_transform_topo_obj,
                    # temporary_files=plant.plant_config.temporary_files,
                    force=self.force,
                    verbose=self.verbose,
                    suffix=suffix)
                plant.append_temporary_file(current_file)
                self.print('geolocated file: %s' % current_file)

        # image outside selected bbox
        elif (not self.test_overlap(projection=self.output_projection,
                                    image_2_obj=image_obj) and
              not image_obj.gcp_list):
            self.print('WARNING file outside limits: %s. Skipping'
                       % current_file)
            return

        # image has GCPs
        elif image_obj.gcp_list:
            self.print('file %s has GCPs' % current_file)
            pass
        else:
            current_file = self._make_gdal_readable(
                current_file,
                flag_apply_transformation,
                flag_test_gdal_open,
                temp_file,
                band)

        # current_file = path.abspath(current_file)
        geo_file = plant.get_temporary_file(
            current_file + '.geo_temp_2',
            dirname=plant.dirname(self.output_file))
        if (self.gdalwarp_list and
                band is not None and
                not self.topo_dir):
            image_obj = self.read_image(current_file, band=band)
            # plant.plant_config.temporary_files.append(geo_file)
            plant.append_temporary_file(geo_file)
            self.save_image(image_obj,
                            geo_file,
                            flag_temporary=True,
                            output_format=plant.DEFAULT_GDAL_FORMAT,
                            force=True)
            image_obj = 0
            projected_file = self._fix_projection(geo_file)
            gdal_filelist.append(projected_file)
        elif self.gdalwarp_list and band is not None:
            print('ERROR option --gdalwarp-list '
                  ' not prepared for slant-range images'
                  ' with multiple bands')
            return
        elif self.gdalwarp_list:
            projected_file = self._fix_projection(current_file)
            gdal_filelist.append(projected_file)
            return
        else:
            bands_list.append(band)
            projected_file = self._fix_projection(current_file)
            geo_file = self.gdalwarp_binding(projected_file,
                                             geo_file,
                                             topo_dir=self.topo_dir)
            # plant.plant_config.temporary_files.append(geo_file)
            plant.append_temporary_file(geo_file)
            gdal_filelist.append(geo_file)

    def _make_gdal_readable(self,
                            current_file,
                            flag_apply_transformation,
                            flag_test_gdal_open,
                            temp_file,
                            band):
        # image needs to be updated (re-saved)
        if (plant.test_for_gdal_convention_error(current_file) or
                flag_apply_transformation or
                self.edges_center is not None or
                self.edges_outer is not None or
                (not flag_test_gdal_open and
                 not plant.is_isce_image(current_file))):
            if plant.test_for_gdal_convention_error(current_file):
                self.print('updating image to follow GDAL '
                           'convention..')
            else:
                self.print('updating image for gdalwarp..')
            with plant.PlantIndent():
                if band is None:
                    image_obj = self.read_image(current_file)
                else:
                    image_obj = self.read_image(current_file,
                                                band=band)
                    band = None
                self.save_image(image_obj,
                                temp_file,
                                flag_temporary=True,
                                output_format=plant.DEFAULT_GDAL_FORMAT,
                                force=True)
            # plant.plant_config.temporary_files.append(temp_file)
            current_file = temp_file

        # image is ISCE and it cannot be opened with GDAL
        elif (not flag_test_gdal_open and
              plant.is_isce_image(current_file)):
            self.print('rendering VRT file for ISCE image..')
            current_file = self.render_vrt(current_file,
                                           temp_file+'.vrt',
                                           edges_center=self.edges_center,
                                           edges_outer=self.edges_outer,
                                           verbose=self.verbose)
            plant.append_temporary_file(current_file)
            self.input_format = 'VRT'
        return current_file

    def _fix_projection(self, input_file,
                        projection=None,
                        output_projection=None):
        if self.topo_dir is not None:
            return input_file

        if projection is None:
            image_obj = plant.read_image(input_file,
                                         read_only=True)
            projection = image_obj.projection
            if projection is None:
                projection = image_obj.gcp_projection

        if output_projection is None:
            output_projection = self.output_projection

        if plant.compare_projections(
                projection, output_projection) is not False:
            # self.print('## *** skipping update')
            return input_file
        self.print(f'INFO updating projection of {input_file}')
        dstSRS = plant.get_projection_proj4(output_projection)
        output_file = plant.get_temporary_file(
            path.basename(input_file + '.geo_temp_4'),
            dirname=plant.dirname(self.output_file),
            append=True)
        output_format = plant.DEFAULT_GDAL_FORMAT
        kwargs = {}

        # srcNodata
        if self.in_null is not None:
            srcNodata = self.in_null
        else:
            srcNodata = 'nan'
        if FLAG_REPROJECT_WITH_GDALWARP:
            gdal_module = plant.Warp
            kwargs['srcNodata'] = srcNodata
            # kwargs['tps'] = srcNodata
            kwargs['dstSRS'] = dstSRS
        else:
            gdal_module = plant.Translate
            kwargs['noData'] = srcNodata
            kwargs['outputSRS'] = dstSRS
        out_image_obj = gdal_module(output_file,
                                    input_file,
                                    # options=options,
                                    flag_return=True,
                                    format=output_format,
                                    # noData=self.in_null,
                                    # GCPs=gcp_list,
                                    # outputBounds=outputBounds,
                                    # xRes=xRes,
                                    # yRes=yRes,
                                    # dstAlpha=dstAlpha,
                                    # outputType=outputType,
                                    # resampleAlg=resampleAlg,
                                    # srcNodata=srcNodata,
                                    # dstNodata=dstNodata,
                                    # cutlineBlend=5,
                                    # cropToCutline=True,
                                    # geoloc=geoloc,
                                    # transformerOptions=transformerOptions)
                                    **kwargs)
        if (out_image_obj.geotransform is None and
            not plant.compare_projections(
                output_projection,
                plant.PROJECTION_REF)):
            print('WARNING there was an error converting'
                  f' the projection from {input_file} to'
                  f' "{dstSRS}". Using default'
                  f' reference instead: "{plant.PROJECTION_REF}"')
            return self._fix_projection(input_file,
                                        projection=projection,
                                        output_projection=plant.PROJECTION_REF)
        elif out_image_obj.geotransform is None:
            print(f'ERROR converting the projection from {input_file} to'
                  f' "{dstSRS}".')
            return
        plant.save_image(out_image_obj, output_file=output_file,
                         force=True)
        return output_file

    def gdalwarp_binding(self, input_files,
                         output_file,
                         topo_dir=None,
                         include_mask_band=True):

        # options = []
        flag_force_float = False
        flag_ctable = None
        # if self.interp_method is None or self.output_dtype is None:
        if self.output_dtype is None:
            flag_force_float = True
            flag_ctable = False
            input_files_loop = input_files
            if not isinstance(input_files, list):
                input_files_loop = [input_files]
            for current_file in input_files_loop:







                # image_obj = self.read_image(current_file,
                # only_header=True)


                # not self because image is already transformed
                image_obj = plant.read_image(current_file,
                                             only_header=True)



                flag_ctable = flag_ctable or image_obj.ctable is not None
                dtype = plant.get_dtype_name(image_obj.dtype).lower()
                if ('int' not in dtype or
                        image_obj.ctable is not None):
                    flag_force_float = False
                if (((self.output_dtype is None and 'int' in dtype) or
                     (self.output_dtype is not None and
                      'int8' in dtype and
                      'int16' in dtype)) and
                        image_obj.ctable is not None):
                    self.output_dtype = image_obj.dtype

        # format
        output_format = plant.DEFAULT_GDAL_FORMAT
        outputBounds = None
        xRes = None
        yRes = None

        # outputBounds
        if (not self.use_gdal_bbox and
                self.bbox is not None and self.step_lat and
                self.step_lon):
            outputBounds = [self.bbox[2]-self.step_lon/2,
                            self.bbox[0]-self.step_lat/2,
                            self.bbox[3]+self.step_lon/2,
                            self.bbox[1]+self.step_lat/2]
        elif (not self.use_gdal_bbox and
              self.bbox is not None):
            outputBounds = [self.bbox[2],
                            self.bbox[0],
                            self.bbox[3],
                            self.bbox[1]]

        # yRes
        if (not self.use_gdal_step and
                self.step_lat):
            yRes = self.step_lat

        # xRes
        if (not self.use_gdal_step and
                self.step_lon):
            xRes = self.step_lon

        dstSRS = plant.get_projection_proj4(self.output_projection)

        # dstAlpha
        dstAlpha = include_mask_band

        # outputType
        force_output_dtype = None
        # self.output_dtype = np.float32
        if self.output_dtype is not None:
            outputType = plant.get_gdal_dtype_from_np(self.output_dtype)
        elif flag_force_float:
            force_output_dtype = image_obj.dtype
            # outputType = 'Float32'
            outputType = plant.get_gdal_dtype_from_np(np.float32)
        else:
            outputType = gdal.GDT_Unknown

        # resampleAlg
        if self.interp_method is None and flag_ctable:
            resampleAlg = 'near'
        elif self.interp_method is None:
            resampleAlg = 'cubic'
        else:
            resampleAlg = self.interp_method
        # options.extend(['-r', resampleAlg])

        # srcNodata
        if self.in_null is not None:
            srcNodata = self.in_null
        else:
            srcNodata = 'nan'

        # dstNodata
        if self.out_null is not None:
            dstNodata = self.out_null
        else:
            dstNodata = 'nan'

        # geoloc
        geoloc = bool(topo_dir)

        # transformerOptions
        transformerOptions = []
        if self.no_geotransform_src:
            transformerOptions.append('SRC_METHOD=NO_GEOTRANSFORM')
        if self.no_geotransform_dst:
            transformerOptions.append('DST_METHOD=NO_GEOTRANSFORM')
        error_message = None
        try:
            image_obj = plant.Warp(output_file,
                                   input_files,
                                   # options=options,
                                   flag_return=True,
                                   format=output_format,
                                   outputBounds=outputBounds,
                                   xRes=xRes,
                                   yRes=yRes,
                                   dstSRS=dstSRS,
                                   dstAlpha=dstAlpha,
                                   outputType=outputType,
                                   resampleAlg=resampleAlg,
                                   srcNodata=srcNodata,
                                   dstNodata=dstNodata,
                                   # cutlineBlend=5,
                                   # cropToCutline=True,
                                   geoloc=geoloc,
                                   transformerOptions=transformerOptions)
        except RuntimeError:
            error_message = plant.get_error_message()
            flag_update_output_bounds = outputBounds is None and self.topo_dir
        if error_message is not None and flag_update_output_bounds:
            plant_grid = plant.PlantGrid(
                bbox=self.bbox,
                step=self.step,
                # bbox_file=input_file,
                verbose=True,
                default_step=None,
                step_lon=self.step_lon,
                step_lat=self.step_lat,
                projection=self.projection,
                bbox_topo=self.topo_dir,
                plant_transform_obj=self.plant_transform_obj)
            plant_grid.projection = self.output_projection
            outputBounds = [plant_grid.bbox[2],
                            plant_grid.bbox[0],
                            plant_grid.bbox[3],
                            plant_grid.bbox[1]]
            image_obj = plant.Warp(output_file,
                                   input_files,
                                   # options=options,
                                   flag_return=True,
                                   format=output_format,
                                   outputBounds=outputBounds,
                                   xRes=xRes,
                                   yRes=yRes,
                                   dstSRS=dstSRS,
                                   dstAlpha=dstAlpha,
                                   outputType=outputType,
                                   resampleAlg=resampleAlg,
                                   srcNodata=srcNodata,
                                   dstNodata=dstNodata,
                                   # cutlineBlend=5,
                                   # cropToCutline=True,
                                   geoloc=geoloc,
                                   transformerOptions=transformerOptions)
        elif error_message is not None:
            raise(error_message)
        
        if force_output_dtype is not None:
            for b in range(image_obj.nbands):
                image = image_obj.get_image(band=b)
                dtype = plant.get_dtype_name(image)
                plant.apply_null(image,
                                 in_null=np.nan,
                                 out_null=plant.INT_NAN)
                image = np.asarray(image, dtype=force_output_dtype)
                image_obj.set_image(image, band=b)
        return image_obj

    def mosaic_no_average(self, gdal_filelist, bands_list):
        self.print('mode: no average')
        for i, current_file in enumerate(reversed(gdal_filelist)):
            if (i == 0):
                self.print('current file: '+current_file)
            else:
                self.print('current file: '+current_file +
                           ' (covering previous data)')

            # open dataset and read parameters
            image_obj = plant.read_image(current_file)
            width = image_obj.width
            length = image_obj.length
            if bands_list[i] is None:
                bands = range(image_obj.nbands-1)
            else:
                bands = [bands_list[i]]
            mask = image_obj.get_image(band=image_obj.nbands-1)
            mask = np.clip(mask, 0, 1)

            data = None

            # transition
            if self.transition > 1:
                from scipy import ndimage
                mask = ndimage.binary_dilation(
                    mask,
                    iterations=int(self.transition))
                mask = ndimage.binary_erosion(
                    mask,
                    iterations=int(self.transition*2)).astype(np.float32)
            if np.sum(mask) == 0:
                continue
            if isinstance(self.factors, list):
                factor = self.factors[i]
            else:
                factor = self.factors
            for current_band_index in bands:
                current_data = image_obj.get_image(band=current_band_index)
                mask = np.logical_and(mask,
                                      plant.isvalid(current_data,
                                                    self.out_null))
                if data is None:
                    data_type = current_data.dtype.name
                    data = np.zeros((length, width),
                                    dtype=data_type)
                    data[:] = self.out_null
                if self.any_null:
                    invalid_orig = plant.isnan(data, self.out_null)
                indexes = np.where(mask)
                data[indexes] = current_data[indexes]*factor
                if self.any_null:
                    invalid_current = plant.isnan(current_data, self.out_null)
                    invalid_indexes = np.where(np.logical_or(invalid_current,
                                                             invalid_orig))
                    data[invalid_indexes] = self.out_null
            # dataset = None
            gdal.ErrorReset()

        if data is None:
            self.print('ERROR no valid elements found')
            sys.exit(0)
        indexes = np.where(plant.isnan(data, self.out_null))
        '''
        if self.output_dtype is None:
            output_dtype = data.dtype.name
        else:
            output_dtype = self.output_dtype
        '''
        plant.insert_nan(data, indexes, self.out_null)
        return data

    def mosaic_average(self, gdal_filelist, bands_list):
        self.print('mode: average')

        # debug(gdal_filelist)

        for i, current_file in enumerate(gdal_filelist):
            self.print(f'current file: {current_file}')
            # open dataset and read parameters

            image_obj = plant.read_image(current_file)
            width = image_obj.width
            length = image_obj.length
            if bands_list[i] is None:
                bands = range(image_obj.nbands-1)
            else:
                bands = [bands_list[i]]
            mask = image_obj.get_image(band=image_obj.nbands-1)

            mask = np.clip(mask, 0, 1)

            # no transition
            if (self.transition//2-1) <= 1:
                indexes = np.where(mask)
                if plant.get_indexes_len(indexes) == 0:
                    continue
            # transition
            else:
                invalid_indexes = np.where(mask == 0)
                from scipy import ndimage
                mask = ndimage.binary_erosion(
                    mask,
                    iterations=int(self.transition //
                                   2-1)).astype(np.float32)
                mask = plant.filter_data(mask,
                                         boxcar=self.transition)
                mask[invalid_indexes] = 0
                indexes = np.nonzero(mask)
                if plant.get_indexes_len(indexes) == 0:
                    continue
            if isinstance(self.factors, list):
                factor = self.factors[i]
            else:
                factor = self.factors

            data = None
            for j, current_band_index in enumerate(bands):
                current_data = image_obj.get_image(band=current_band_index)
                if data is None:
                    data_type = current_data.dtype.name
                    data = np.zeros((length, width), dtype=data_type)
                    if not self.transition:
                        ndata = np.zeros((length, width), dtype=np.uint16)
                    else:
                        ndata = np.zeros((length, width), dtype=np.float32)
                if self.any_null:
                    invalid_orig = plant.isnan(data, self.out_null)

                # no transition
                if (self.transition//2-1) <= 1:
                    data[indexes] += current_data[indexes]*factor
                    ndata[indexes] += 1
                else:
                    data[indexes] += (current_data[indexes]*mask[indexes] *
                                      factor)
                    ndata += mask
                if self.any_null:
                    invalid_current = plant.isnan(current_data, self.out_null)
                    invalid_indexes = np.where(np.logical_or(invalid_current,
                                                             invalid_orig))
                    ndata[invalid_indexes] = 0
                    data[invalid_indexes] = self.out_null
            # dataset = None
            gdal.ErrorReset()

        if data is None:
            self.print('ERROR no valid elements found')
            sys.exit(0)
        if not self.math_mode:
            indexes = np.nonzero(ndata)
            data[indexes] = data[indexes]/ndata[indexes]
        indexes = np.where(ndata == 0)
        '''
        if self.output_dtype is None:
            output_dtype = data.dtype.name
        else:
            output_dtype = self.output_dtype
        '''
        plant.insert_nan(data, indexes, self.out_null)
        return data



def main(argv=None):
    with plant.PlantLogger():
        parser = get_parser()
        self_obj = PlantMosaic(parser, argv)
        ret = self_obj.run()
        return ret


if __name__ == '__main__':
    main()
