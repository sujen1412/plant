import sys, numpy
#import progressbar

class Polygon:
    """
    Implement class Polygon
    """
    def __init__(self, *args):
        """
        Instantiate object Polygon and add coordinates.
        If 1 argument is given, it is assumed that it's a list of (x, y) pairs.
        If 2 arguments are given, it is assumed that they are a list of X coordinates and a list of Y coordinates.

        >>> p1 = Polygon([(0, 0), (10, 0), (10, 10), (0, 10)])
        >>> x,y = p1.x, p1.y
        >>> print x, y
        [0, 10, 10, 0] [0, 0, 10, 10]
        >>> print p1.path
        [(0, 0), (10, 0), (10, 10), (0, 10)]
        >>> print p1.nbVertices
        4
        >>> print p1.boundary
        (0, 0, 10, 10)

        >>> p2 = Polygon(x, y)
        >>> print p2.x, p2.y
        [0, 10, 10, 0] [0, 0, 10, 10]
        >>> print p2.path
        [(0, 0), (10, 0), (10, 10), (0, 10)]
        >>> print p2.nbVertices
        4
        >>> print p2.boundary
        (0, 0, 10, 10)
        """
        nbArgs = len(args)
        if nbArgs == 1:
            poly = args[0]
            self.path = poly
            self.x, self.y = poly2vect(poly)
        elif nbArgs == 2:
            polX = args[0]
            polY = args[1]
            self.x, self.y = polX, polY
            self.path = vect2poly(polX, polY)
        else:
            sys.exit("To instantiate the polygon, please give 1 argument as a list of (x, y) pairs OR 2 arguments as a list of X coordinates and a list of Y coordinates.")
        self.nbVertices = len(self.x)
        self.boundary = min(self.x), min(self.y), max(self.x), max(self.y)

    def isRectangle(self):
        """
        Check if a polygon is a rectangle

        >>> polys = [Polygon([0, 2], [0, 10]), Polygon([0, 2, 5, 1], [0, 1, 4, 3]), Polygon([20, 20, 15, 15], [3, 9, 9, 3])]
        >>> result = []
        >>> for p in polys:
        ...     result.append(p.isRectangle())
        ...
        >>> print result
        [False, False, True]

        :return: whether polygon is a rectangle or not
        :rtype: boolean
        """
        if self.nbVertices != 4:
            return False
        Xmin, Ymin, Xmax, Ymax = self.boundary
        p = self.path
        for x in [Xmin, Xmax]:
            for y in [Ymin, Ymax]:
                if (x, y) not in p:
                    return False
        return True

    def getBoundary(self):
        """
        Return boundary as offsetX, offsetY, width, height

        >>> poly = Polygon([(0, 0), (10, 0), (10, 15), (0, 10)])
        >>> print poly.getBoundary()
        (0, 0, 11, 16)

        :return coordinates of outer box containing polygon
        :rtype: tuple
        """
        Xmin, Ymin, Xmax, Ymax = self.boundary
        return Xmin, Ymin, (Xmax - Xmin + 1), (Ymax - Ymin + 1)

    
    def pnpoly(self, x, y):
        """
        Determine if a point is inside a given polygon or not.
        Polygon is represented by a list of X coordinates and a list of Y coordinates.
        Return True if (x, y) is inside polygon.
        Code adapted from: http://local.wasp.uwa.edu.au/~pbourke/geometry/insidepoly/

        >>> polX = [0, 10, 10, 0]
        >>> polY = [0, 0, 10, 10]
        >>> p = Polygon(polX, polY)
        >>> points = [(10, 0), (15, 0), (-2, 0), (0, -1), (0, 10), (0, 13), (5, 5), (2, 9), (15, 20)]
        >>> result = []
        >>> for (x, y) in points:
        ...     result.append(p.pnpoly(x, y))
        ...
        >>> print result
        [True, False, False, False, True, False, True, True, False]

        :param x: x coordinate of point to be checked
        :type x: numeric
        :param y: y coordinate of point to be checked
        :type y: numeric
        :return: whether point is inside polygon or not
        :rtype: boolean
        """
        polX, polY = self.x, self.y
        npol = self.nbVertices
        inside = False
        i = 0
        j = npol - 1
        while i < npol:
            if (((polY[i] == y) and (polX[i] == x)) or
                ((polY[j] == y) and (polX[j] == x))):
                return True
            if ((((polY[i] <= y) and (y < polY[j])) or
                 ((polY[j] <= y) and (y < polY[i]))) and
                (x < (polX[j] - polX[i]) * (y - polY[i]) / (polY[j] - polY[i]) + polX[i])):
                inside = not inside
            j = i
            i = i + 1
        return inside

    def pnboundary(self):
        """
        Take all points within the outer box of polygon
        and check if they are contained in polygon
        (= return True if inside polygon)

        >>> poly = Polygon([0, 1, 2, 3, 4, 4, 2, 0], [0, 0, 1, 0, 0, 2, 4, 2])
        >>> print poly.pnboundary()
        [True, True, False, True, True, True, True, True, True, False, True, True, True, True, True, False, True, True, False, False, False, False, True, False, False]

        :return: True if point of outer box is INside polygon
        :rtype: list of boolean
        """
        offsetX, offsetY, width, height = self.getBoundary()
        insides = []
        for j in range(height):
            for i in range(width):
                insides.append(self.pnpoly(offsetX + i, offsetY + j))
        return insides

    def inside(self):
        """
        Alias for pnboundary
        """
        return self.pnboundary()

    def outside(self):
        """
        Take all points within the outer box of polygon
        and check if they are not contained in polygon
        (= return True if outside of polygon)

        >>> poly = Polygon([0, 1, 2, 3, 4, 4, 2, 0], [0, 0, 1, 0, 0, 2, 4, 2])
        >>> print poly.outside()
        [False, False, True, False, False, False, False, False, False, True, False, False, False, False, False, True, False, False, True, True, True, True, False, True, True]

        :return: True if point of outer box is OUTside polygon
        :rtype: list of boolean
        """
        offsetX, offsetY, width, height = self.getBoundary()
        outsides = []
        for j in range(int(height)):
            for i in range(int(width)):
                outsides.append(not self.pnpoly(offsetX + i, offsetY + j))
        return outsides

    def extractData(self, raster, dt, nCol):
        '''
        Return a masked array of dt values from raster, located under polygon.
        Non valid numerics are masked too.

        >>> import os, tempfile
        >>> dt = numpy.dtype('c8')
        >>> na = numpy.array([numpy.complex64(complex(i, 1.0 * (i-1))) for i in range(100)])
        >>> tempFD, tempName = tempfile.mkstemp('.tmp')
        >>> f = os.fdopen(tempFD, 'w')
        >>> na.tofile(f)
        >>> f.close()
        >>> p1 = createRectangle(1, 0, 3, 5)
        >>> p2 = Polygon([0, 1, 2, 3, 4, 4, 2, 0], [0, 0, 1, 0, 0, 2, 4, 2])
        >>> data1 = p1.extractData(tempName, 'c8', 10)
        >>> data2 = p2.extractData(tempName, 'c8', 10)
        >>> os.remove(tempName)
        >>> print data1
        [(1+0j), (2+1j), (3+2j), (11+10j), (12+11j), (13+12j), (21+20j), (22+21j), (23+22j), (31+30j), (32+31j), (33+32j), (41+40j), (42+41j), (43+42j), (51+50j), (52+51j), (53+52j)]
        >>> print data2
        [-1j, (1+0j), (3+2j), (4+3j), (10+9j), (11+10j), (12+11j), (13+12j), (20+19j), (21+20j), (22+21j), (23+22j), (24+23j), (31+30j), (32+31j), (42+41j)]
        '''
        window = [] #use of list is faster than array
        dt = numpy.dtype(dt)
        mult = dt.itemsize
        offsetCol, offsetRow, width, height = self.getBoundary()
        f = open(raster)
        for h in range(0, int(height)):
            f.seek((nCol * (offsetRow + h) + offsetCol) * mult) #reading entire file at once is worth it only if polygon covers large portion of file extent
            w = numpy.fromfile(f, dtype=dt, count=int(width))
            window += w.tolist() #concatenation of lists is way faster than that of arrays
        f.close()
        window = numpy.ma.masked_invalid(window)
        if not self.isRectangle():
            outsides = self.outside()
            window = numpy.ma.masked_where(outsides, window)
        return window.compressed().tolist()

    def print2bin(self, filename, ncol, nrow):
        data = []
        for y in range(nrow):
            for x in range(ncol):
                if self.pnpoly(x, y):
                    data.append(numpy.uint8(255))
                else:
                    data.append(numpy.uint8(0))
        f = open(filename, 'wb')
        numpy.array(data).tofile(f)
        f.close()

## OUTSIDE CLASS
## -------------


def poly2vect(poly):
    """
    Convert a list of (x, y) pairs to a pair of lists (vectX, vectY).
    >>> poly = [(0, 0), (10, 0), (10, 10), (0, 10)]
    >>> x, y = poly2vect(poly)
    >>> print x, y
    [0, 10, 10, 0] [0, 0, 10, 10]

    :param poly: vertices of a polygon
    :type poly: list of (x,y)
    :return: list of x coordinates and list of y coordinates
    :rtype: tuple
    """
    vectX, vectY = [], []
    for i in range(len(poly)):
        x, y = poly[i]
        vectX.append(x)
        vectY.append(y)
    return vectX, vectY


def vect2poly(vectX, vectY):
    """
    Convert a pair of lists (vectX, vectY) to a list of (x, y) pairs.

    >>> vectX = [0, 10, 10, 0]
    >>> vectY = [0, 0, 10, 10]
    >>> poly = vect2poly(vectX, vectY)
    >>> print poly
    [(0, 0), (10, 0), (10, 10), (0, 10)]

    :param vectX: list of x coordinates of vertices
    :type vectX: list
    :param vectY: list of y coordinates of vertices
    :type vectY: list
    :return: list of vertices of polygon
    :rtype: list of tuple
    """
    poly = []
    for i in range(len(vectX)):
        poly.append((vectX[i], vectY[i]))
    return poly

