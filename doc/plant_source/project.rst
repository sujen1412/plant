Project Summary
================

Introduction
---------------

Here we explain basic elements of PLAnT’s framework, including scripts/apps, main objects, user interface, exception handling, logging and testing. 

We start with the basic interaction of the user with PLAnT, which is through Python programing (including interactive mode and python notebooks) and bash commands. To import PLAnT, just use “import plant”. All plant modules and constants are accessible through plant.<complement>. Most of PLAnT methods are located in the “modules” folder, organized by the type of function. For instance, radar functions are located in modules/plant_radar.py.

PLAnT’s constants are defined in /modules/plant_constants.py. The constants that can be set by the user are located in the configuration file: plant_config.txt.


Image, band and transform
------------------------------------------

The data, handled by the framework, is mostly represented by a PlantImage class. It contains a set of bands, which is represented by a PlantBand class, and attributes that are common to the data. The attributes include data dimensions, geo-reference (if available) and the recipe to open the image. The data is only loaded from the file if there is an explicit request to access its content. Part of this recipe is contained in the PlantTransform object.

The data content is obtained by the read_image() method. Many drivers are available to open the data, including GDAL, PANDAS, ISCE supported formats, and also direct binary access and virtual files. The content is organized by a set of PlantBand class, each containing 2D or 3D arrays. It also includes support for points (as a 1x1 2D array) and vectors (as 1xN 2D array). The bands (points, vector or rasters) of a same image must have the same dimensions and geo-reference (if available). 

PLAnT has a built-in image manipulation that can be store as “recipe” and applied when data is accessed. The class responsible for that is the PlantTransform.py. A transformation class can be particular to a single image or be shared by other images of the script. 

Scripts (or apps)
------------------------------

The bash commands are called **scripts** or **apps**. To take advantage of PLAnT’s framework, the script module should inherit from PlantScript class and follow a standard, that can be verified by looking at one of the scripts located in the “app” folder. We also added a template (app/plant_template.py) if one wants to create its own template. The script module should contain the argparse declaration. Since many of the radar/remote sensing parameters are similar between functions, many default parameters are available. A more detailed description of PLAnT scripts will be given in the documentation of PlantScript class. 

Every script with the name starting with the prefix “plant\_” is automatically wrapped to a callable method. For example, plant_util.py is also accessible as x=plant.plant_util(). The arguments are the same defined in the argparse (variable name or alias) and the result is usually returned as a PlantImage instance (in this case to the variable x).

The broadcasting of environment attributes, i.e. attributes common to an execution, is controlled by a wrapper contained in the PlantScript. For instance, if method is called within PlantScript context, i.e. self.read_image(), the transform object of the script is automatically loaded, otherwise, i.e. plant.read_image(), only the transformed object passed in the method call will be used. Similarly, self.save_image() will automatically import attributes contained in the environment (self), if they were not explicitly passed through the function call. As an example, if the output_file parameter is not set, the wrapper will look for self.output_file, if still not found, the method default will be used instead.


Input/output, exception handling and logging
----------------------------------------------

PLAnT input/output, exception handling and logging is performed by PlantLogger.py. We added a wrapper to the standard input and output that allow us to log all messages and to add colors the output text. For instance, if the output message starts with WARNING, this text will be printed in a different color (yellow for dark background). 

Ideally, PLAnT logger is started in the script module outside the script class definition with the use of python “with” statement. This allows PLAnT to more robustly handle session ending. However, if PlantLogger is not explicitly called, it will be automatically created.

If the execution leaves the “with” statement, or if an exception is raised or the output text starts with ERROR (which in PLAnT also raises an exception), the __exit__ method is called. This method handles the exiting procedures, such as deleting temporary files, closing log file, printing the location where the log was saved, etc. The 

We also added the debugging capability that shows the traceback for each printed line. Also, the plant,.debug() will print messages only if the debug mode is activated.

The activation/desactivation of logging and debug mode, the logging location (locally or in a fixed directory), the number of traceback levels are set in the configuration file (plant_config.txt).

Testing
------------------------------------------

PLAnT testing is done with pytest. The tests are located in the “tests” folder. The pytest checks can be called with the command:

>>> python -m pytest -v

Further tests are provided with the script plant_test.py also located in the “tests” folder.
