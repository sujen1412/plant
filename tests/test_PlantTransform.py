import numpy as np
import plant
import pytest
from config_tests import SHAPEFILES_IMAGES


@pytest.mark.parametrize("img", SHAPEFILES_IMAGES)
def test_shapefiles(img):
    print(f'test shapefiles: {img}')
    image_obj = plant.read_image(img)
    assert image_obj is not None
    if image_obj.nbands == 1:
        assert image_obj.geotransform is not None
        assert np.nanmean(image_obj.image > 0)
        assert len(image_obj.shape) == 2
    else:
        for b in range(image_obj.nbands):
            assert np.nanmean(image_obj.get_image(band=b)) != 0
