#!/usr/bin/env python3
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
# Author: Federico Croce, Gustavo H. X. Shiroma, Marco Lavalle
# Copyright 2010-2011, by the California Institute of Technology. ALL RIGHTS
# RESERVED. United States Government Sponsorship acknowledged.
# Any commercial use must be negotiated with the Office of Technology Transfer
# at the California Institute of Technology.
#
# This software may be subject to U.S. export control laws. By accepting this
# software, the user agrees to comply with all applicable U.S.
# export laws and regulations. User has the responsibility to obtain export
# licenses, or other export authority as may be required before exporting such
# information to foreign countries or providing access to foreign persons.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

import plant
import pytest
import os
import random
from config_tests import SLC_IMAGES
#    JPG_IMAGES, \
#    ENVI_IMAGES, \
#    S3_IMAGES, \
#    VRT_IMAGES

OUT_FILENAME = "output"

SUPPORTED_EXTENSIONS_FOR_GEOCODE = ["vrt", "slc", "kmz"]


def geotransform_image(img):
    img_dir = os.path.dirname(img)
    for ext in SUPPORTED_EXTENSIONS_FOR_GEOCODE:
        out_filename = OUT_FILENAME + '.' + ext
        rnd_step = random.uniform(0.001, 0.005)
        for square_flag in [True, False]:
            for db_flag in [True, False]:
                plant.geocode(img,
                              topo_dir=img_dir,
                              output_dir=img_dir,
                              output=img_dir + out_filename,
                              step=rnd_step,
                              square=square_flag,
                              db=db_flag)
                os.remove(img_dir + out_filename)


@pytest.mark.parametrize("img", SLC_IMAGES)
def test_mosaic_from_slc(img):
    geotransform_image(img)

