FROM ubuntu:latest

LABEL Federico <federico.croce@jpl.nasa.gov>

RUN apt-get update
RUN apt-get -y install wget
RUN apt-get -y install git
RUN wget https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh -O ~/miniconda.sh

RUN bash ~/miniconda.sh -b -p /miniconda
ENV PATH $PATH:/miniconda/bin/

RUN conda init
RUN conda create --name plant
ENV PATH /miniconda/envs/plant/bin:$PATH
WORKDIR /root/home
RUN conda install conda-build
COPY . /root/home/plant
RUN conda build -n plant plant
RUN conda install -n plant --use-local plant -y
RUN echo "source activate plant" >> ~/.bashrc

CMD ["/bin/bash"]

